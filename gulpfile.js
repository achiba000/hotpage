'use strict';

var SCSS_SRC = "./assets/sass/**/*.scss";
var CSS_DEST = "./css";

var gulp = require('gulp');
var sass = require('gulp-sass');

// Sassコンパイルタスク
gulp.task('sass-compile', function(){
  return gulp.src(SCSS_SRC)
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest(CSS_DEST));
});

// scssファイル群の変更を監視するタスク
gulp.task('scss:watch', function(){
  var watcher = gulp.watch(SCSS_SRC, ['sass-compile']);
  watcher.on('change', function(event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
});

// gulpのデフォルトタスクとしてscss:watchタスクを指定
gulp.task('default', ['scss:watch']);
